This project is an implementation of text files interpretator.

use your console:

# make build directory
mkdir build
cd build

# generate projects
cmake ..

# build exe and unittests
cmake --build .
```
To start work with the interpretator add some commands in a input.txt
and then put the input.txt in the build directory.

input file contains following instructions:
1. create circle <id> <color> <x> <y> <radius>
2. create rectangle <id> <color> <x> <y> <w> <h>
3. create square <id> <color> <x> <y> <side>
4. create triangle <id> <color> <x> <y> <a> <b>
5. set color <id> <color>
6. move <id> <dx> <dy>
7. scale <id> <scale>
8. bring to back <id>
9. bring to front <id>
10. draw <filename> <scene_x> <scene_y> <scene_w> <scene_h> <width> <height>

Now you can use the interpretator.

# ./interpretator
check output file <filename> that is written in the input.txt


