#include <deque>
#include <string>
#include <memory>

struct Object {

  std::string type;

  int ID;

  int color;

  double x;

  double y;

  virtual ~Object() {}

};

struct Circle : Object {
  double radius;
};

struct Rectangle : Object {
  double width;
  double height;
};

struct Square : Object {
  double side;
};

struct Triangle : Object {
  double a;
  double b;
};


class Scene {

  std::deque<std::shared_ptr<Object>> objects;

public:

  Scene();

  void create_circle(int ID, int color, double x, double y, double radius);

  void create_rectangle(int ID, int color, double x, double y, double width, double height);

  void create_square(int ID, int color, double x, double y, double side);

  void create_triangle(int id, int color, double x, double y, double a, double b);

  void set_color(int ID, int color);

  void move(int ID, double dx, double dy);

  void scale(int ID, double scale);

  void bring_to_back(int ID);

  void bring_to_front(int ID);

  void draw(std::ostream& os, double scene_x, double scene_y, double scene_width, double scene_height, int width, int height);

  void checkID(int iD);

  void checkColor(int color);

  void checkPositiveness(double side, int ID, std::string type);

  int returnColor(int ID);

  double returnCoordinate_x(int ID);

  double returnCoordinate_y(int ID);

  double returnRadius(int ID);

  double returnHeight(int ID);

  double returnWidth(int ID);

  double returnSide(int ID);

  double returnA(int ID);

  double returnB(int ID);
};
