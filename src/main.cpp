#include <iostream>
#include <fstream>
#include <vector>
#include "Scene.cpp"

using std::ifstream;
using std::string;
using std::stoi;
using std::stod;

int main(int argc, char** argv) {

  string input_filename;
  if (argc == 2) {
    input_filename = argv[1];
  } else {
    std::cerr << "Enter exactly one input file" << std::endl;
    return 1;
  }

  Scene session;
  ifstream  ifs(input_filename.c_str());

  if (!ifs) {
    std::cerr << "ERROR: cannot open " << input_filename << " file" << std::endl;
    return 1;
  }

  string command;
  while (ifs >> command) {

    if (command == "create") {
      string object;
      ifs >> object;
      if (object == "circle") {
        string ID, color, x, y, radius;
        ifs >> ID >> color >> x >> y >> radius;
          try {
            session.create_circle(stoi(ID), stoi(color), stod(x), stod(y), stod(radius));
          } catch(std::invalid_argument& e) {
              std::cerr << "Cannot parse create_circle" << std::endl;
              std::cerr << e.what() << std::endl;
              return 1;
          }
      }

      else if (object == "rectangle") {
        string ID, color, x, y, width, height;
        ifs >> ID >> color >> x >> y >> width >> height;
        try {
          session.create_rectangle(stoi(ID), stoi(color), stod(x), stod(y), stod(width), stod(height));
        } catch(std::invalid_argument& e) {
          std::cerr << "Cannot parse create_rectangle" << std::endl;
          std::cerr << e.what() << std::endl;
          return 1;
        }
      }

      else if (object == "square") {
        string ID, color, x, y, side;
        ifs >> ID >> color  >> x >> y >> side;
        try {
          session.create_square(stoi(ID), stoi(color), stod(x), stod(y), stod(side));
        } catch(std::invalid_argument& e) {
          std::cerr << "Cannot parse create_square" << std::endl;
          std::cerr << e.what() << std::endl;
          return 1;
        }
      }

      else if (object == "triangle") {
        string ID, color, x, y, a, b;
        ifs >> ID >> color >> x >> y >> a >> b;
        try {
          session.create_triangle(stoi(ID), stoi(color), stod(x), stod(y), stod(a), stod(b));
        } catch(std::invalid_argument& e) {
          std::cerr << "Cannot parse create_triangle" << std::endl;
          std::cerr << e.what() << std::endl;
          return 1;
        }
      }

      else {
        std::cerr << "Mistaken command: " << command << " " << object << std::endl;
        return 1;
      }
    }


    if (command == "set") {
      string what;
      ifs >> what;
      if (what == "color") {
        string ID, color;
        ifs >> ID >> color;
        try {
          session.set_color(stoi(ID), stoi(color));
        } catch(std::invalid_argument& e) {
          std::cerr << "Cannot parse set_color" << std::endl;
          std::cerr << e.what() << std::endl;
          return 1;
        }
      }
      else {
        std::cerr << "Mistaken command: " << command << " " << what << std::endl;
        return 1;
      }
    }


    if (command == "move") {
      string ID, dx, dy;
      ifs >> ID >> dx >> dy;
      try {
        session.move(stoi(ID), stod(dx), stod(dy));
      } catch(std::invalid_argument& e) {
        std::cerr << "Cannot parse move" << std::endl;
        std::cerr << e.what() << std::endl;
        return 1;
      }
    }


    if (command == "scale") {
      string ID, scal;
      ifs >> ID >> scal;
      try {
        session.scale(stoi(ID), stod(scal));
      } catch(std::invalid_argument& e) {
        std::cerr << "Cannot parse scale" << std::endl;
        std::cerr << e.what() << std::endl;
        return 1;
      }
    }


    if (command == "bring") {
      string to;
      string what;
      ifs >> to >> what;
      if (to == "to") {
        if (what == "back") {
          string ID;
          ifs >> ID;
          try {
            session.bring_to_back(stoi(ID));
          } catch(std::invalid_argument& e) {
            std::cerr << "Cannot parse bring_to_back" << std::endl;
            std::cerr << e.what() << std::endl;
            return 1;
          }
        }

        else if (what == "front") {
          string ID;
          ifs >> ID;
          try {
            session.bring_to_front(stoi(ID));
          } catch(std::invalid_argument& e) {
            std::cerr << "Cannot parse bring_to_front" << std::endl;
            std::cerr << e.what() << std::endl;
            return 1;
          }
        }
        else {
          std::cerr << "Mistaken command: " << command << " " << to << " " << what << std::endl;
          return 1;
        }
      }
      else {
        std::cerr << "Mistaken command: " << command << " " << to << " " << what << std::endl;
        return 1;
      }
    }


    if (command == "draw") {
      string filename;
      string scene_x, scene_y, scene_width, scene_height, width, height;
      ifs >> filename >> scene_x >> scene_y >> scene_width >> scene_height >> width >> height;
      std::ofstream ofs(filename.c_str());
      try {
        session.draw(ofs, stod(scene_x), stod(scene_y), stod(scene_width), stod(scene_height), stod(width), stod(height));
      } catch(std::invalid_argument& e) {
        std::cerr << "Cannot parse draw" << std::endl;
        std::cerr << e.what() << std::endl;
        return 1;
      } catch(std::runtime_error& e) {
        std::cerr << "Cannot open file " << filename << std::endl;
      }
    }
  }


  return 0;
}
