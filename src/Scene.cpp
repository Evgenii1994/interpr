#include "Scene.h"
#include <stdexcept>
#include <fstream>
#include <sstream>
#include <iostream>
#include <memory>
#include <string>

void Scene::checkID(int ID) {
  for (auto& object : objects) {
    if (object->ID == ID) {
      std::string str = std::to_string(ID);
      throw std::invalid_argument("Already have such ID : " + str);
    }
  }
}

void Scene::checkColor(int color) {
  if (color < 0 || color > 4) {
    std::string str = std::to_string(color);
    throw std::invalid_argument("Wrong color : " + str);
  }
}

void Scene::checkPositiveness(double side, int ID, std::string type) {
  if (side < 0) {
    std::string str = std::to_string(side);
    std::string str1 = std::to_string(ID);
    if (type == "circle") {
      throw std::invalid_argument("ID: " + str1 + " Wrong circle radius : " + str);
    }
    if (type == "rectangle") {
      throw std::invalid_argument("ID: " + str1 + " Wrong rectangle width/height : " + str);
    }
    if (type == "square") {
      throw std::invalid_argument("ID: " + str1 + " Wrong square side : " + str);
    }
    if (type == "triangle") {
      throw std::invalid_argument("ID: " + str1 + " Wrong triangle a/b : " + str);
    }
  }
}

Scene::Scene() {}

int Scene::returnColor(int ID) {
  for (auto& obj : objects) {
    if (obj->ID == ID) {
      return obj->color;
    }
  }
}

double Scene::returnCoordinate_x(int ID) {
  for (auto& obj : objects) {
    if (obj->ID == ID) {
      return obj->x;
    }
  }
}

double Scene::returnCoordinate_y(int ID) {
  for (auto& obj : objects) {
    if (obj->ID == ID) {
      return obj->y;
    }
  }
}

double Scene::returnRadius(int ID) {
  for (auto& obj : objects) {
    if (obj->ID == ID) {
      auto downcastedPtr = std::dynamic_pointer_cast<Circle>(obj);
      return downcastedPtr->radius;
    }
  }
}

double Scene::returnHeight(int ID) {
  for (auto& obj : objects) {
    if (obj->ID == ID) {
      auto downcastedPtr = std::dynamic_pointer_cast<Rectangle>(obj);
      return downcastedPtr->height;
    }
  }
}

double Scene::returnWidth(int ID) {
  for (auto& obj : objects) {
    if (obj->ID == ID) {
      auto downcastedPtr = std::dynamic_pointer_cast<Rectangle>(obj);
      return downcastedPtr->width;
    }
  }
}

double Scene::returnSide(int ID) {
  for (auto& obj : objects) {
    if (obj->ID == ID) {
      auto downcastedPtr = std::dynamic_pointer_cast<Square>(obj);
      return downcastedPtr->side;
    }
  }
}

double Scene::returnA(int ID) {
  for (auto& obj : objects) {
    if (obj->ID == ID) {
      auto downcastedPtr = std::dynamic_pointer_cast<Triangle>(obj);
      return downcastedPtr->a;
    }
  }
}

double Scene::returnB(int ID) {
  for (auto& obj : objects) {
    if (obj->ID == ID) {
      auto downcastedPtr = std::dynamic_pointer_cast<Triangle>(obj);
      return downcastedPtr->b;
    }
  }
}

void Scene::create_circle(int ID, int color, double x, double y, double radius) {
  checkID(ID);
  checkColor(color);
  checkPositiveness(radius, ID, "circle");
  auto obj = std::make_shared<Circle>();
  obj->type = "circle";
  obj->ID = ID;
  obj->color = color;
  obj->x = x;
  obj->y = y;
  obj->radius = radius;
  objects.push_back(std::move(obj));
}

void Scene::create_rectangle(int ID, int color, double x, double y, double width, double height) {
  checkID(ID);
  checkColor(color);
  checkPositiveness(width, ID, "rectangle");
  checkPositiveness(height, ID, "rectangle");
  auto obj = std::make_shared<Rectangle>();
  obj->type = "rectangle";
  obj->ID = ID;
  obj->color = color;
  obj->x = x;
  obj->y = y;
  obj->width = width;
  obj->height = height;
  objects.push_back(std::move(obj));
}

void Scene::create_square(int ID, int color, double x, double y, double side) {
  checkID(ID);
  checkColor(color);
  checkPositiveness(side, ID, "square");
  auto obj = std::make_shared<Square>();
  obj->type = "square";
  obj->ID = ID;
  obj->color = color;
  obj->x = x;
  obj->y = y;
  obj->side = side;
  objects.push_back(std::move(obj));
}

void Scene::create_triangle(int ID, int color, double x, double y, double a, double b) {
  checkID(ID);
  checkColor(color);
  checkPositiveness(a, ID, "triangle");
  checkPositiveness(b, ID, "triangle");
  auto obj = std::make_shared<Triangle>();
  obj->type = "triangle";
  obj->ID = ID;
  obj->color = color;
  obj->x = x;
  obj->y = y;
  obj->a = a;
  obj->b = b;
  objects.push_back(std::move(obj));
}

void Scene::set_color(int ID, int color) {
  checkColor(color);
  int flag = 0;
  for (auto& obj : objects) {
    if (obj->ID == ID) {
      obj->color = color;
      flag = 1;
    }
    if (flag == 0) {
      std::string str = std::to_string(ID);
      throw std::invalid_argument("No such ID : " + str);
    }
  }
}

void Scene::move(int ID, double dx, double dy) {
  int flag = 0;
  for (auto& obj : objects) {
    if (obj->ID == ID) {
      obj->x += dx;
      obj->y += dy;
      flag = 1;
    }
    if (flag == 0) {
      std::string str = std::to_string(ID);
      throw std::invalid_argument("No such ID : " + str);
    }
  }
}

void Scene::scale(int ID, double scale) {
  if (scale <= 0) {
    std::string str = std::to_string(scale);
    throw std::invalid_argument("Wrong scale argument : " + str);
  }
  int flag = 0;
  for (auto& obj : objects) {

    if (obj->ID == ID) {
      flag = 1;

      if (obj->type == "circle") {
        auto cir = std::dynamic_pointer_cast<Circle>(obj);
        cir->radius *= scale;
      }

      if (obj->type == "rectangle") {
        auto rec = std::dynamic_pointer_cast<Rectangle>(obj);
        double centr_mass_x = rec->x + rec->width / 2;
        double centr_mass_y = rec->y + rec->height / 2;
        rec->x = rec->x - centr_mass_x;
        rec->y = rec->y - centr_mass_y;
        rec->x *= scale;
        rec->y *= scale;
        rec->x = rec->x + centr_mass_x;
        rec->y = rec->y + centr_mass_y;
        rec->width *= scale;
        rec->height *= scale;
      }

      if (obj->type == "square") {
        auto squ = std::dynamic_pointer_cast<Square>(obj);
        double centr_mass_x = squ->x + squ->side / 2;
        double centr_mass_y = squ->y + squ->side / 2;
        squ->x = squ->x - centr_mass_x;
        squ->y = squ->y - centr_mass_y;
        squ->x *= scale;
        squ->y *= scale;
        squ->x = squ->x + centr_mass_x;
        squ->y = squ->y + centr_mass_y;
        squ->side *= scale;
      }

      if (obj->type == "triangle") {
        auto tri = std::dynamic_pointer_cast<Triangle>(obj);
        double centr_mass_x = (3 * tri->x + tri->a) / 3;
        double centr_mass_y = (3 * tri->y + tri->b) / 3;
        tri->x = tri->x - centr_mass_x;
        tri->y = tri->y - centr_mass_y;
        tri->x *= scale;
        tri->y *= scale;
        tri->x = obj->x + centr_mass_x;
        tri->y = obj->y + centr_mass_y;
        tri->a *= scale;
        tri->b *= scale;
      }
    }
  }
  if (flag == 0) {
    std::string str = std::to_string(ID);
    throw std::invalid_argument("No such ID : " + str);
  }
}

void Scene::bring_to_back(int ID) {
  int flag = 0;
  for (int i = 0; i < objects.size(); ++i) {
    if (objects[i]->ID == ID) {
      flag = 1;
      auto obj = std::make_shared<Object>();
      obj = objects[i];
      objects.erase(objects.begin() + i);
      objects.push_front(obj);
      break;
    }
  }
  if (flag == 0) {
    std::string str = std::to_string(ID);
    throw std::invalid_argument("No such ID : " + str);
  }
}

void Scene::bring_to_front(int ID) {
  int flag = 0;
  for (int i = 0; i < objects.size(); ++i) {
    if (objects[i]->ID == ID) {
      flag = 1;
      auto obj = std::make_shared<Object>();
      obj = objects[i];
      objects.erase(objects.begin() + i);
      objects.push_back(obj);
      break;
    }
  }
  if (flag == 0) {
    std::string str = std::to_string(ID);
    throw std::invalid_argument("No such ID : " + str);
  }
}

void Scene::draw(std::ostream& ofs, double scene_x, double scene_y, double scene_width, double scene_height, int width, int height) {
  if (!ofs)
  {
    throw std::runtime_error("");
  }


  double Y = scene_y + height;
  double X = scene_x;
  for (int pixel = 0; pixel < width * height; ++pixel) {
    if (pixel % width == 0 ) {
      ofs << std::endl;
      --Y;
      X = scene_x;
    }

    int what_to_print = 1;
    for (auto& obj : objects) {
      if (obj->type == "circle"){
        auto cir = std::dynamic_pointer_cast<Circle>(obj);
        if (((X - cir->x) * (X - cir->x) + (Y - cir->y) * (Y - cir->y)) <= cir->radius * cir->radius) {
          what_to_print = cir->color;
        }
      }

      if (obj->type == "rectangle") {
        auto rec = std::dynamic_pointer_cast<Rectangle>(obj);
        if (X >= rec->x && Y >= rec->y && X <= rec->x + rec->width && Y <= rec->y + rec->height) {
          what_to_print = rec->color;
        }
      }

      if (obj->type == "square") {
        auto squ = std::dynamic_pointer_cast<Square>(obj);
        if (X >= squ->x && Y >= squ->y && X <= squ->x + squ->side && Y <= squ->y + squ->side) {
          what_to_print = squ->color;
        }
      }

      if (obj->type == "triangle") {
        auto tri = std::dynamic_pointer_cast<Triangle>(obj);
        double X1 = X - tri->a;
        double Y1 = Y;
        double X2 = X;
        double Y2 = Y - tri->b;
        double a = (X1 - tri->x) * (Y2 - Y1) - (X2 - X1) * (Y1 - tri->y);
        double b = (X2 - tri->x) * (Y - Y2) - (X - X2) * (Y2 - tri->y);
        double c = (X - tri->x) * (Y1 - Y) - (X1 - X) * (Y - tri->y);
        if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0)) {
          what_to_print = tri->color;
        }
      }
    }
    if (what_to_print == 0) {
      ofs << ' ';
    }
    if (what_to_print == 1) {
      ofs << '.';
    }
    if (what_to_print == 2) {
      ofs << 'o';
    }
    if (what_to_print == 3) {
      ofs << '*';
    }
    ++X;
  }
}
