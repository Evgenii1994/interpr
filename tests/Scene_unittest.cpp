#include "gtest/gtest.h"
#include <sstream>
#include <iostream>
#include "Scene.h"


TEST(Interpretator, Create_Circle_and_Set_Color)
{
  Scene session;
  session.create_circle(1, 1, 1.0, 2.0, 5.0);
  session.set_color(1, 2);
  ASSERT_EQ(2, session.returnColor(1));
}

TEST(Interpretator, Create_Rectangle_and_Set_Color)
{
  Scene session;
  session.create_rectangle(1, 1, 1.0, 2.0, 5.0, 10.0);
  session.set_color(1, 2);
  ASSERT_EQ(2, session.returnColor(1));
}

TEST(Interpretator, Create_Square_and_Set_Color)
{
  Scene session;
  session.create_square(1, 1, 1.0, 2.0, 5.0);
  session.set_color(1, 2);
  ASSERT_EQ(2, session.returnColor(1));
}

TEST(Interpretator, Create_Triangle_and_Set_Color)
{
  Scene session;
  session.create_triangle(1, 1, 1.0, 2.0, 5.0, 10.0);
  session.set_color(1, 2);
  ASSERT_EQ(2, session.returnColor(1));
}

TEST(Interpretator, Create_Circle_and_move)
{
  Scene session;
  session.create_circle(1, 1, 1.0, 2.0, 5.0);
  session.move(1, 5.0, 7.0);
  ASSERT_EQ(6.0, session.returnCoordinate_x(1));
  ASSERT_EQ(9.0, session.returnCoordinate_y(1));
}

TEST(Interpretator, Create_Rectangle_and_move)
{
  Scene session;
  session.create_rectangle(1, 1, 1.0, 2.0, 5.0, 10.0);
  session.move(1, 5.0, 7.0);
  ASSERT_EQ(6.0, session.returnCoordinate_x(1));
  ASSERT_EQ(9.0, session.returnCoordinate_y(1));
}

TEST(Interpretator, Create_Square_and_move)
{
  Scene session;
  session.create_square(1, 1, 1.0, 2.0, 5.0);
  session.move(1, 5.0, 7.0);
  ASSERT_EQ(6.0, session.returnCoordinate_x(1));
  ASSERT_EQ(9.0, session.returnCoordinate_y(1));
}

TEST(Interpretator, Create_Triangle_and_move)
{
  Scene session;
  session.create_triangle(1, 1, 1.0, 2.0, 5.0, 10.0);
  session.move(1, 5.0, 7.0);
  ASSERT_EQ(6.0, session.returnCoordinate_x(1));
  ASSERT_EQ(9.0, session.returnCoordinate_y(1));
}

TEST(Interpretator, Create_Circle_and_scale)
{
  Scene session;
  session.create_circle(1, 1, 1.0, 2.0, 5.0);
  session.scale(1, 2.0);
  ASSERT_EQ(10.0, session.returnRadius(1));
}

TEST(Interpretator, Create_Rectangle_and_scale)
{
  Scene session;
  session.create_rectangle(1, 1, 1.0, 2.0, 5.0, 10.0);
  session.scale(1, 2.0);
  ASSERT_EQ(10.0, session.returnWidth(1));
  ASSERT_EQ(20.0, session.returnHeight(1));
  ASSERT_EQ(-1.5, session.returnCoordinate_x(1));
  ASSERT_EQ(-3.0, session.returnCoordinate_y(1));
}

TEST(Interpretator, Create_Square_and_scale)
{
  Scene session;
  session.create_square(1, 1, 1.0, 2.0, 5.0);
  session.scale(1, 2.0);
  ASSERT_EQ(10.0, session.returnSide(1));
  ASSERT_EQ(-1.5, session.returnCoordinate_x(1));
  ASSERT_EQ(-0.5, session.returnCoordinate_y(1));
}

TEST(Interpretator, Create_Triangle_and_scale)
{
  Scene session;
  session.create_triangle(1, 1, 1.0, 2.0, 5.0, 10.0);
  session.scale(1, 2.0);
  ASSERT_EQ(10.0, session.returnA(1));
  ASSERT_EQ(20.0, session.returnB(1));
  ASSERT_EQ(-1.5, session.returnCoordinate_x(1));
  ASSERT_EQ(-3.0, session.returnCoordinate_y(1));
}

TEST(Interpretator, Draw_and_Bring_to_front_and_Bring_to_back)
{
  Scene session;
  session.create_square(1, 3, 1.0, 2.0, 5.0);
  session.create_square(2, 2, 3.0, 4.0, 5.0);
  std::string str1 = "\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "...oooooo.....................\n"
  "...oooooo.....................\n"
  ".**oooooo.....................\n"
  ".**oooooo.....................\n"
  ".**oooooo.....................\n"
  ".**oooooo.....................\n"
  ".******.......................\n"
  ".******.......................\n"
  "..............................\n"
  "..............................";

  std::ostringstream ss1;
  session.draw(ss1, 0.0, 0.0, 15.0, 15.0, 30.0, 30.0);
  std::string str = ss1.str();
  ASSERT_EQ(str1, str);

  session.bring_to_front(1);
  std::string str2 = "\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "..............................\n"
  "...oooooo.....................\n"
  "...oooooo.....................\n"
  ".******oo.....................\n"
  ".******oo.....................\n"
  ".******oo.....................\n"
  ".******oo.....................\n"
  ".******.......................\n"
  ".******.......................\n"
  "..............................\n"
  "..............................";
  std::ostringstream ss2;
  session.draw(ss2, 0.0, 0.0, 15.0, 15.0, 30.0, 30.0);
  str = ss2.str();
  ASSERT_EQ(str2, str);

  session.bring_to_back(1);
  std::ostringstream ss3;
  session.draw(ss3, 0.0, 0.0, 15.0, 15.0, 30.0, 30.0);
  str = ss3.str();
  ASSERT_EQ(str1, str);
}
